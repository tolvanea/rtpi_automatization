help_text = """
This script reads observables from rtpi-code, and extracts information. 
Electron density should work for both 2d and 3d.

With this you can:
    - print mean energy ('Etot') after covergion
    - plot energy by time step
    - plot density heat map
    - plot radial density
    - animate density distribution over time steps 
    
Note: PIMC-automatization package must be in $PYTHONPATH, so that its modules
can be imported here directly.
(https://gitlab.com/tolvanea/PIMC-automatization)
Note, you should have file fock_darwin_energy_states.py in same directory

Usage:
    Print energy mean with automatic convergence check:
        $ python3 read_some_observables.py
        
    Plot energy and wavefunction 
        $ python3 read_some_observables.py plot
        
    Plot energy and wavefunction and animate wave function
        $ python3 read_some_observables.py animate
         
    Plot energy and wavefunction, but do it in discrete figures for latex images
        $ python3 read_some_observables.py discrete_figures
        
    
    If you want to fix calculation convergion point to N (instead of automatic 
    determination), make (empty) file with name "FIX_CONVERGED_S=N". 
    
Yes I know this code is bad. It is quite ugly "all-in-main" approach.
"""
import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib import animation
import sys
from uncertainties import ufloat  # pip install [--user] uncertainties
import scipy.ndimage as ndimage
import os

# PIMC-automatization repo is in path
from read_data.auto_convergence_detection import calculate_converged_point_2
import read_data.utilities.sem2 as sem2
from miscellaneous import general_tools  # pretty_print_errorbars
from miscellaneous.conf_modification import read_line

# Relative to this directory
import fock_darwin_energy_states

# square box (2d or 3d)
xmin = -2.0  # grid_lower_corner
xmax = 2.0  #grid_upper_corner
density_size = (64, 64)

def read_energy():
    filename = './observables.h5'
    filename_walker = './walker.h5'
    f = h5py.File(filename, 'r')
    f_walker = h5py.File(filename_walker, 'r')

    E_tot = f["E"]["Etot"]
    N_max = int(E_tot.attrs["N"])  # Sometimes some calculations are left out
    N = 0
    valid = np.full(N_max, fill_value=False)
    for block in range(0, N_max):
        has_key_1 = "PSI{:04}".format(block+1) in f_walker["PSI"]
        has_key_2 = "R{:04}".format(block+1) in f_walker["R"]
        if (has_key_1 and has_key_2):
            N += 1
            valid[block] = True

    # If no fixed S found from file "FIX_CONVERGED_S=N", then calculate it automatically.
    for filename in os.listdir("."):
        if filename.startswith("FIX_CONVERGED_S="):
            try:
                S = int(filename[16:])
            except ValueError:
                print("Inconsistency with start position, is it format 'FIX_CONVERGED_S=XX'?")
                exit(0)
            break
    else:
        S = calculate_converged_point_2(E_tot, N, assume_converged=0.2)

    E_tot = E_tot[:N_max][valid]
    assert N > 5 or N_max > 5
    assert len(E_tot) > 5
    assert S < N
    assert N <= N_max
    return E_tot ,S, N, N_max

def index_in_grid(r, shape, lower_corner, upper_corner):
    box_size = np.array(upper_corner) - np.array(lower_corner)
    r_u = (r - lower_corner) / box_size  # scaled to unit box
    idx = (r_u * shape).astype(int)

    if idx.min() < 0 or (idx-shape).max() >= 0:
        return None
    else:
        return idx

def read_wavefunctions(N, N_max):
    """
    Reads walkers and return 2d (sum)projection of wavefunction with 16 time steps.
    :param N:
    :return:
    """
    filename = './walker.h5'
    f = h5py.File(filename, 'r')
    dim = None

    if os.path.isfile(f'wavefunction_(N:{N_max}).npy'):
        grid = np.load(f'wavefunction_(N:{N_max}).npy')
        return grid
    else:
        previous_wavefunction_file = None
        for filename in reversed(os.listdir(".")):
            if filename.startswith("wavefunction_(N:"):
                previous_wavefunction_file = filename
                break

        grid = np.zeros((*density_size,N_max), dtype=np.complex128)
        N_start = 0
        N_from_filename = 0

        if previous_wavefunction_file is not None:
            prev_grid = np.load(previous_wavefunction_file)
            N_start = prev_grid.shape[2]

            try:
                N_from_filename = int(previous_wavefunction_file[16:-5])
            except ValueError:  # Int conversion
                raise RuntimeError("You renamed wavefunction_(N:xx).npy file. Shame on you, not on my lazy code.")
            grid[:,:,:N_start] = prev_grid


        bad_blocks = 0
        for block in range(N_from_filename, N_max):
            print("block", block ,"/", N_max)
            try:
                wf_walkers = f["PSI"]["PSI{:04}".format(block+1)][...]
                r_walkers = f["R"]["R{:04}".format(block+1)][...]
            except KeyError:
                print("No results found with block " + str(block))
                bad_blocks += 1
                continue

            dim = len(r_walkers[0,0,:])
            low = np.array([xmin for _ in range(dim)])
            upp = np.array([xmax for _ in range(dim)])

            for i in range(wf_walkers.shape[1]):
                wf = wf_walkers[:,i]
                #print(r_walkers[i,:])
                r = r_walkers[i, 0, :]
                wf = wf[0] + 1j*wf[1]

                if r.shape[0] == 3:
                    r = r[:2]  # Flatted third dimension

                idx = index_in_grid(r, grid.shape[:2], low, upp)
                if idx is not None:
                    id = block - N_from_filename + N_start - bad_blocks
                    np.put(
                        grid[:,:,id],
                        np.ravel_multi_index(idx, grid.shape[0:2]),
                        grid[:,:,id].item(*idx) + wf
                    )
        print("done")
        # Normalize so that integral over probability density is 1
        # Normalize in vectorized manner for each block
        dV = ((low[:2] - upp[:2]) / np.array(density_size)).prod()
        grid = grid[:,:,:N]  # Crop shitty values off
        normalization = np.sqrt((np.abs(grid)**2).sum(axis=(0,1)) * dV)
        assert not np.any(normalization==0.0)
        normed = (grid / normalization).astype(np.complex64)
        np.save('wavefunction_(N:{}).npy'.format(N_max), normed)
        if previous_wavefunction_file is not None:
            os.remove(previous_wavefunction_file)
        return (normed)




def calc_radial_density(grid, std, sum_count, box_dims, center_point):
    """
    Calculate radial density along the center point of N-dimensional density (N>=2)
    (General dimensional code may not be the easiest to read. Sorry about that.)
    :param grid:            density
    :param std:             starnard deviation grid of density
    :param box_dims:        length of simulation box as a tupple
    :param center_point:    point around which radial density is calculated. None for automatic
                            calculation determination from mean density
    :return:
        x_range,        1d-grid of radial points
        rad,            1d-density
        rad_err,        1d-density error
        center_point    if 'center_point=None', return this
    """
    dV = (np.array(box_dims) / np.array(grid.shape)).prod()
    assert len(grid.shape) == len(box_dims)
    if center_point is None:  # Find out center of density
        coord_grid_origo = []
        for i in range(len(grid.shape)):
            a = box_dims[i]/2
            x = np.linspace(-a, a, grid.shape[i], endpoint=False)
            dx = (x[1]-x[0])
            coord_grid_origo.append(x+dx/2)
        coords_origo = np.meshgrid(*coord_grid_origo)
        center_point = []
        for i, coord in enumerate(coords_origo):
            mean = (coord * grid).sum(axis=tuple(range(len(grid.shape)))) * dV
            center_point.append(mean)
    else:
        assert len(box_dims) == len(center_point)
    #assert len(box_dims) < 2 or len(box_dims) > 3
    coord_grid = []
    for i in range(len(grid.shape)):
        a = box_dims[i]/2
        x = np.linspace(-a, a, grid.shape[i], endpoint=False)
        dx = (x[1]-x[0])
        x += dx/2 - center_point[i]  # Center of density pixel
        coord_grid.append(x)
    coords = np.stack(np.meshgrid(*coord_grid), axis=0)
    dists = np.sqrt((coords**2).sum(axis=0)).flatten()

    mindim = np.argmin(box_dims)
    l = box_dims[mindim]/2
    d = grid.shape[mindim]
    x_range = np.linspace(0, l, d//2, endpoint=False)
    rad = np.zeros(d//2)
    rad_err1 = np.zeros(d//2)  # Radial error from block (=time) deviation
    x_bin_count = np.zeros(d//2, dtype=np.int_)
    bin = [[] for i in range(d//2)]
    for dist, dens, dev in zip(dists, grid.flat, std.flat):
        idx = index_in_grid(dist, x_range.shape, (0,), (l,))
        if idx is not None:
            rad[idx[0]] += dens
            rad_err1[idx[0]] += dev**2 / sum_count
            x_bin_count[idx[0]] += 1
            bin[idx[0]].append(dens)
    x_bin_count[x_bin_count==0] = 1 ## No zero division
    assert x_bin_count.sum() > 0
    assert (x_bin_count[x_bin_count==0].sum() == 0)
    rad /= x_bin_count
    # error a+b propagate quadratically: sqrt(a^2 + b^2)
    rad_err1 = np.sqrt(rad_err1) / x_bin_count

    # Normalize so that integral over sphere is 1
    # (Even though it should be already almost normalized, but it's not?!)
    dx = x_range[1] - x_range[0]
    norm = (rad * x_range * dx).sum()
    rad /= norm

    # Errorbars
    rad_err2 = np.zeros(d//2)  # Radial error from non-circularly symmetric deviation
    itrt = map(lambda x: np.std(x)/np.sqrt(len(x)), bin)
    for i, sigma in enumerate(itrt):
        rad_err2[i] = sigma/norm

    # Assume these two errors are independent, even though they aren't.
    # (The second is direct consequence of the first.)
    rad_err = np.sqrt(rad_err1**2 + rad_err2**2) * 2  # 2 sigma = 97%

    return x_range, rad, rad_err, center_point

def analytical_dens(x_range, p, l, ω_0, B):
    mass = 1.0

    dx = (x_range[1] - x_range[0])  # pixel width

    dens = np.linspace(0, 1.5, len(x_range))
    norm = 0.0
    for i, x in enumerate(x_range):
        wf = fock_darwin_energy_states.Ψ_cartesian(x + dx/2, 0, p, l, mass, ω_0, B)
        prob = np.abs(wf)**2
        norm += x*dx * prob
        dens[i] = prob

    dens /= norm
    return dens

def minimize_single_param_function(residual_fun, guess, guess_range):
    """
    Finds real parameter for which residual vector function is most close to zero vector.
    (Minimizes the error)
    This does two iterations with brute force search on grid of size 2048. Resolution is something
    like 1/4 000 000 of size of guess_range.

    example for residual function:
    def residual(a):
        x = np.linspace(0,1,6)
        data = np.array([0,2,3,1,-1,])
        model_data = a*x**2
        return (model_data**2).sum()

    Args:
        residual_fun    Vector valued residual function,Function that return same shape of vector as data.
                        That is: 'f(guess) -> np.ndarray' where 'guess' is real
        guess           initial guess for parameter
        guess_range     [min, max] range of values that can be give to guess.
    :return:
    """
    import numpy as np
    import scipy.optimize as optimize
    import matplotlib.pylab as plt

    minimum = float("inf")
    min_id = 1000000

    x_ax = np.linspace(guess_range[0], guess_range[1], 2048)

    for i, a in enumerate(x_ax):
        x = residual_fun(a)
        assert x >= 0  # residual_fun must give positive values
        if x < minimum:
            minimum = x
            min_id = i

    x_ax2 = np.linspace(x_ax[max(0,min_id-1)], x_ax[min(2048,min_id+1)], 2048)

    for i, a in enumerate(x_ax2):
        x = residual_fun(a) # residual_fun must give positive values
        assert x >= 0
        if x < minimum:
            minimum = x
            min_id = i

    return x_ax2[min_id]


def main():

    command = None
    if len(sys.argv) > 1:
        command = sys.argv[1]
        if command in ["help", "--help", "-h"]:
            print(help_text)
            return
    elif len(sys.argv) > 2:
        print(help_text)
        raise Exception("Too many arguments")

    E_tot, S, N, N_max = read_energy()
    E_mean, E_err = sem2.sem2(E_tot, S, N)
    E = ufloat(E_mean, E_err)
    if N > 2:
        s = general_tools.pretty_print_errorbars(E.n, E.s)
    else:
        s = str(E.n)
    print("E_tot: {}".format(s))

    if command is None:
        return

    if command not in ["plot", "animate", "discrete_figures"]:
        raise Exception("Unknown argument " + command)

    # Plot
    _, filename = os.path.split(os.getcwd())
    if command == "discrete_figures":
        fig1 = None
        f1, ax1 = plt.subplots(1, 1, figsize=(4, 4), num="1{}".format(filename))
        f2, ax2 = plt.subplots(1, 1, figsize=(4, 4), num="2{}".format(filename))
        f3, ax3 = plt.subplots(1, 1, figsize=(4, 4), num="3{}".format(filename))
        axs = [ax1, ax2, ax3]
    else:
        fig1, axs = plt.subplots(1, 3, figsize=(12, 4), num="{}".format(filename))

    wf_grid = read_wavefunctions(N, N_max)
    prob = np.abs(wf_grid)**2
    #smoothened = ndimage.gaussian_filter(prob, sigma=(0,0,0), order=0)
    flat_dens = prob[:,:,S:].mean(axis=2)
    flat_std =  prob[:,:,S:].std(axis=2)
    axs[0].plot(list(range(0,S+1)), E_tot[0:S+1], linestyle=":", c="C0")
    axs[0].plot(list(range(S,N)), E_tot[S:N], c="C0")
    axs[1].imshow(np.flipud(flat_dens), extent=(xmin, xmax, xmin, xmax))
    # Compare with analytical
    x, rad, rad_err, center = calc_radial_density(
        flat_dens, flat_std, prob.shape[2]-S, (xmax - xmin, xmax - xmin), None)
    ω_0 = float(read_line("CONF", "Omega")[0])
    try:
        B = float(read_line("CONF", "MagneticFluxDensity")[0])
    except RuntimeError:
        B = 0.0
    l = 0
    p = 0

    residual = lambda ω: ((rad - analytical_dens(x, p, l, ω, B))**2).sum()
    ω_fit = minimize_single_param_function(residual, 1.0, [0.1, 30])

    axs[2].errorbar(x, rad, rad_err, label="RTPI")
    anl = analytical_dens(x, p, l, ω_0, B)
    if B > 1e-10:
        anl2 = analytical_dens(x, p, l, ω_0, 0.0)
        label = "Expected dens. $\\omega_0={:.2f}$, $B={}$"
        label_0 = "(No mgn. field $\\omega_0={:.2f}$)"
        axs[2].plot(x, anl, label=label.format(ω_0,int(B)), ls="--", c="C2")
        axs[2].plot(x, anl2, label=label_0.format(ω_0), ls=":", c="C4")
    else:
        anl2 = analytical_dens(x, p, l, ω_fit, 0.0)
        #anl3 = analytical_dens(x, p, l, 5.4, 0.0)
        label_fit = "Curve fit with $\\omega_0={:.2f}$"
        label =     "Expected dens. $\\omega_0={:.2f}$"
        axs[2].plot(x, anl2, label=label_fit.format(ω_fit), ls=":", linewidth=3)
        axs[2].plot(x, anl, label=label.format(ω_0), ls="--", c="C2")
        #axs[2].plot(x, anl3, label=label.format(l,p,5.4), ls=":", c="C3")
    # for pi, p in enumerate([0, 1]):
        # for li, l in enumerate([0, 1]):
        #     ls = ["-",":"][li]
        #     anl = analytical_dens(x, p, l, ω_0)
        #     axs[2].plot(x, anl, label="FD n={}, k={}, $\\omega_0={:.2f}$".format(l,p,ω_0), ls=ls)
        #     if li == 1 and pi > 1:
        #         continue
    axs[2].legend()

    axs[0].set_title("Energy")
    axs[0].set_xlabel("simulation time")
    axs[0].set_ylabel("$E_{tot}$")
    axs[2].set_title("Radial density")
    axs[2].set_xlabel("$r$")
    axs[2].set_ylabel("$\\rho$")

    if command == "discrete_figures":
        f1.tight_layout()
        f2.tight_layout()
        f3.tight_layout()
    else:
        axs[1].scatter(*center, marker="+", color="r")
        axs[1].set_title("Density")
        fig1.savefig(f"density_plot.pdf")

    if command in ["animate"]:
        # animate
        # 2d Density
        #fig2, ax2 = plt.subplots(1, 1, figsize=(5, 4), num="{} animation".format(filename))
        fig2, (ax2, ax3) = plt.subplots(1, 2, figsize=(8, 4), num="{} animation".format(filename))
        #fig3, ax3 = plt.subplots(1, 1, figsize=(5, 4), num="{} radial animation".format(filename))
        img = ax2.imshow(np.flipud(prob[:,:,-1]), extent=(xmin, xmax, xmin, xmax))

        # Radial density
        p = 0
        container = ax3.errorbar(
            x, rad, rad_err, label="RTPI", capsize=3, elinewidth=-0.001,
        )
        ax3.plot(x, anl2, label="Curve fit with $\\omega_0={:.2f}$".format(l,p,ω_fit), ls=":")
        ax3.plot(x, anl, label="Expected dens. $\\omega_0={:.2f}$".format(l,p,ω_0), ls="--")
        line, (bottoms, tops), (verts,) = container
        text = ax2.text(1.4, 1.7, f't:{0}', color="white", horizontalalignment="left")
        ax3.legend()

        def animate(i):
            b = max(0,i-1)  # begin
            e = min(N-1, i+2)
            # Small break after looping again
            if i >= N:
                # 2D density
                img.set_data(np.zeros(prob[:,:,0].shape)*float("nan"))

                # Radial density
                line.set_ydata(np.zeros(len(rad))*float("nan"))
                bottoms.set_ydata(np.zeros(len(rad))*float("nan"))
                tops.set_ydata(np.zeros(len(rad))*float("nan"))
            else:
                # 2D density
                img.set_data(np.flipud(prob[:,:,i]))
                text.set_text(f't:{i}')

                # Radial density
                flat_dens_2 = prob[:,:,b:e].mean(axis=2)
                flat_std_2 = prob[:,:,b:e].std(axis=2)
                _, rad2, rad_err2, center = calc_radial_density(
                    flat_dens_2, flat_std_2, e-b, (xmax - xmin, xmax - xmin), None
                )
                top = rad2 + rad_err2
                bot = rad2 - rad_err2
                line.set_ydata(rad2)
                bottoms.set_ydata(bot)
                tops.set_ydata(top)
                if False:
                    ar = np.vstack((top, bot)).T
                    print(ar.shape)
                    # Found yet another matplotlib bug. Yay!
                    # No seriously, fuck matplotlib
                    verts.set_verts(ar)
            return img, line, bottoms, tops, verts, text
        anim = animation.FuncAnimation(fig2, animate, frames=N+2, interval=33,)
        # ffmpeg or mencoder need to be installed.
        anim.save('density_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
        print("Video encoding done.")

    plt.show()
main()
