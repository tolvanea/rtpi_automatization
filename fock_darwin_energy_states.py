"""
This file calculates analytically Fock-Darvin (FD) energy states (i.e. 2d parabolic QD in magnetic field)
https://arxiv.org/pdf/1703.06634.pdf

Also check result by calculating same from wikipedia article without magnetic field.
https://en.wikipedia.org/wiki/Quantum_harmonic_oscillator#cite_note-1

This may be used as a module, but it can be executed directly when it
    - prints excitation energies in table format for FD
    - plots FD electron densities with different excitations
    - Plots side by side FD and harmonic oscillator densities

TODO Fock-Darwin B=0 and harmonic oscillator does not match?
     This is probably due to the fact that the formula diverges on B->0
TODO What is the correct formula for FD-wave functions? The references does not match 100%
    https://arxiv.org/pdf/1703.06634.pdf
    https://qdev.nbi.ku.dk/student_theses/BachelorThesisMariekeVanBeest.pdf
    λ
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.special import assoc_laguerre

ℏ = 1.0
c = 137.035999084
e = 1.0

xmin = -2.0
xmax = 2.0


def E_mn(m, n, γ, ω):
    return (m*(1+γ) + n*(1-γ) + 1) * ℏ * ω

def E_pl(p, l, γ, ω):
    """
    This (m,n) differs from (p,l) so that different equation is used from source
    https://arxiv.org/pdf/1703.06634.pdf
    """
    ω_c = γ * (2*ω)
    return (2*p + np.abs(l) + 1)*ℏ*ω - 0.5*ℏ*ω_c

def E_kn(k, n, γ, ω):
    """
    This uses energy equation from
    https://qdev.nbi.ku.dk/student_theses/BachelorThesisMariekeVanBeest.pdf

    There must be mistake, because this equation does not converge to correct
    energies on B=0.
    """
    ω_c = γ * (2*ω)
    ω_1 = ω_c / 2
    return ℏ*ω_1 * ((2*k + np.abs(n) + 1)/ ω + n)

def print_table(mass, ω_0, B, E_fun, mn=True):
    ω_c = e * B / (mass * c)
    ω = np.sqrt(ω_c**2 / 4 + ω_0**2)
    γ = ω_c / (2*ω)

    if mn:
        print("m\\n ", end="")
    else:
        print("p\\l ", end="")
    for n in range(4):
        print(n, end="      ")
    print("")
    for m in range(4):
        print("{}   ".format(m), end="")
        for n in range(4):
            print("{:.3f}".format(E_fun(m, n, γ, ω)), end=", ")
        print("")

def Ψ(ρ, φ, p, l):
    # MITÄ V*** ONKO TÄSSÄ VIRHE?!? Ei exponenttiin kuulu "+ 0.5".
    # Alkuperäisessä lähteessä on oltava virhe
    # Uusi lähde: https://qdev.nbi.ku.dk/student_theses/BachelorThesisMariekeVanBeest.pdf
    # Vai onko tässäkin virhe? Ks lähde:
    # http://www.fisica.unige.it/~mesoscop/Theses/DeGiovannini_PhD.pdf
    # TODO there should probably be no " + np.abs(l)" in laguerres parameter
    return np.exp(1j*(-l)*φ - (ρ**2)/2) * ρ**(np.abs(l)) * assoc_laguerre(ρ**2, p + np.abs(l), np.abs(l))

    #return np.exp(1j*l*φ - (ρ**2)/2) * ρ**(np.abs(l) + 0.5) * assoc_laguerre(ρ**2, p, np.abs(l))

def Ψ_cartesian(x,y, p, l, mass, ω_0, B):
    ω_c = e * B / (mass * c)
    ω = np.sqrt(ω_c**2 / 4 + ω_0**2)

    ρ = np.sqrt(mass * ω) * np.sqrt(x**2 + y**2)
    if x == 0.0:
        φ = np.pi * (1 - 0.5*np.sign(y))
    else:
        φ = np.arctan(y/x)
    return Ψ(ρ, φ, p, l)

def calculate_wavefunction_grid(p, l, mass, ω_0, B, Ψ_fun=None):
    if Ψ_fun is None:
        Ψ_fun = Ψ

    ω_c = e * B / (mass * c)
    ω = np.sqrt(ω_c**2 / 4 + ω_0**2)

    grid = np.zeros((128, 128,), dtype=np.complex128)
    x_range = np.linspace(xmin, xmax, 128)
    y_range = np.linspace(xmin, xmax, 128)

    for i, y in enumerate(y_range):
        for j, x in enumerate(x_range):
            ρ = np.sqrt(mass * ω) * np.sqrt(x**2 + y**2)
            if x == 0.0:
                φ = np.pi * (1 - 0.5*np.sign(y))
            else:
                φ = np.arctan(y/x)

            grid[i,j] = Ψ_fun(ρ, φ, p, l)

    return grid, x_range


def mn(p,l):
    """
    convert quantum numbers p, l to m, n
    """
    if l >= 0:
        return(p, l+p)
    else:
        return(p-l, p)


def compare_with_wikipedia(mass, ω_0, B=0.0):
    def E_mn_NO_B(m, n, γ, ω):
        return ℏ * ω*(n + m + 0.5)

    def Ψ_1d_NO_B(x, n):
        """  x in is in natural units (same as ρ). No magnetic field B."""
        coeff = [0.0 for i in range(n)] + [1.0]
        H_n = np.polynomial.hermite.Hermite(coeff)
        normalization = 1 / np.sqrt(2**n * np.math.factorial(n)) * np.pi**(-1/4)
        return normalization * np.exp(-x**2 / 2) * H_n(x)

    def Ψ_2d_NO_B(ρ, φ, p, l):
        #print("TODO FIX THIS TEST")
        m, n = mn(p, l)
        x = np.cos(φ) * ρ
        y = np.sin(φ) * ρ
        # sum = 0.0                                       #
        # for m in range(l+1):                            #
        #     n = l-m                                     #
        #     sum += Ψ_1d_NO_B(x, m) * Ψ_1d_NO_B(y, n)    #
        # return sum                                      #
        return Ψ_1d_NO_B(x, m) * Ψ_1d_NO_B(y, n)

    def Ψ_2d_NO_B_version_2(ρ, φ, p, l):
        """ 
        https://www.physicspages.com/pdf/Shankar/Shankar%20Exercises%2010.02.02%20-%2010.02.03.pdf
        """
        m, n = mn(p, l)
        base = np.exp(-ρ**2/2)
        if m == n == 0:
            return base
        elif m == 1 and n == 0:
            return base * ρ * np.cos(φ)
        elif m == 0 and n == 1:
            return base * ρ * np.sin(φ)
        else:
            #return float("nan")
            raise NotImplementedError


    # Magnetic field is now 0.
    print("Exitation energies\n")
    print("B=0, Fock-Darwin (m,n)")
    print_table(mass, ω_0, B, E_fun=E_mn)
    print("")

    print("B=0, Fock-Darwin (k,n)")
    print_table(mass, ω_0, B, E_fun=E_kn)
    print("")

    print("B=0, wikipedia equation")
    print_table(mass, ω_0, B, E_fun=E_mn_NO_B)
    print("")

    def draw_fig1():
        """
        Compares Fock-Darwin and wikipedia
        """
        fig1, axs = plt.subplots(2, 4, figsize=(12, 6), num="FD vs wikipedia")
        #fig1, axs = plt.subplots(2, 2, figsize=(8, 8), num="FD vs wikipedia")

        fig1.suptitle("Fock-Darwin (FD) and quantum dot (QD)")
        def plot_4(row, Ψ_fun, label, states, mass, ω_0, B,):
            for i, state in enumerate(states):
                wf, _ = calculate_wavefunction_grid(*state, mass, ω_0, B, Ψ_fun=Ψ_fun)
                prob = np.abs(wf)**2
                axs[row,i].imshow(prob, extent=(xmin,xmax,xmin,xmax))
                axs[row,i].set_title(f"{label}, n={state[0]}, k={state[1]}")

        # (1,0), (0,1), (
        states = ((0,0), (0,1) ,(1,0), (1,1))  # pairs of (m,n)
        # Fock darwin
        plot_4(0, None, "FD", states, mass, ω_0, B)
        # Wikipedia equation
        plot_4(1, Ψ_2d_NO_B, "FD", states, mass, ω_0, B)

    def draw_fig2():
        """
        Compares wikipedia and some other source
        """
        fig2, ax_5_6 = plt.subplots(1, 2, figsize=(8, 4), num="wikipedia vs other source")
        p = 0
        l = 1

        wf_QD_v1, _ = calculate_wavefunction_grid(p, l, mass, ω_0, B, Ψ_fun=Ψ_2d_NO_B)
        wf_QD_v2, _ = calculate_wavefunction_grid(p, l, mass, ω_0, B, Ψ_fun=Ψ_2d_NO_B_version_2)
        prob_QD_v1 = np.abs(wf_QD_v1)**2
        prob_QD_v2 = np.abs(wf_QD_v2)**2

        fig2.suptitle("p={}, l={}".format(p,l))
        ax_5_6[0].set_title("2d QD from widipedia")
        ax_5_6[0].imshow(prob_QD_v1, extent=(xmin,xmax,xmin,xmax))
        ax_5_6[1].set_title("2d QD from other source")
        ax_5_6[1].imshow(prob_QD_v2, extent=(xmin,xmax,xmin,xmax))

    def draw_fig3():
        """
        Compares ground state with fock darwin wikipedia, and some other source
        """
        fig3, ax_7 = plt.subplots(1, 1, figsize=(4, 4), num="1d plot")

        p, l = 0, 0
        wf_FD_v0, x = calculate_wavefunction_grid(p, l, mass, ω_0, B)
        wf_QD_v1, _ = calculate_wavefunction_grid(p, l, mass, ω_0, B, Ψ_fun=Ψ_2d_NO_B)
        wf_QD_v2, _ = calculate_wavefunction_grid(p, l, mass, ω_0, B, Ψ_fun=Ψ_2d_NO_B_version_2)

        prob_FD_v0 = np.abs(wf_FD_v0)**2
        prob_QD_v1 = np.abs(wf_QD_v1)**2
        prob_QD_v2 = np.abs(wf_QD_v2)**2

        ax_7.plot(x, prob_FD_v0[64,:] / prob_FD_v0.sum(), c="C1", label="Fock-Darwin")
        ax_7.plot(x, prob_QD_v1[64,:] / prob_QD_v1.sum(), c="C0", label="Quantum Dot v1", ls="--")
        ax_7.plot(x, prob_QD_v2[64,:] / prob_QD_v2.sum(), c="C3", label="Quantum Dot v2", ls=":")
        ax_7.legend()


    draw_fig1()
    draw_fig2()
    draw_fig3()


def main():
    ω_0 = 1.0  # parabole confinement strength
    mass = 1.0
    fig, axs = plt.subplots(2, 4, figsize=(12, 6), num="magnetic field and existed states")

    compare_with_wikipedia(mass, ω_0, B=0)

    for i, B in enumerate([0.0, 1000.0]):  # 'B' is magnetic field strength
        print("B={:.3f} (p,l)".format(B))
        print_table(mass, ω_0, B, E_fun=E_pl, mn=False)
        print("")
        for li, l in enumerate([0, 1]):
            for pi, p in enumerate([1, 2]):
                wf, _ = calculate_wavefunction_grid(p, l, mass, ω_0, B)
                prob = np.abs(wf)**2
                axs[i][2*li + pi].imshow(prob, extent=(xmin,xmax,xmin,xmax))
                axs[i][2*li + pi].set_title("B = {}, n = {}, k = {}".format(B, l ,p))

    plt.show()

if __name__ == "__main__":
    main()
