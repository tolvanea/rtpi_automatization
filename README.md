# rtpi_automatization

Automates plotting and data extraction from RTPI-code (Real Time Path Integral) developed in Electron Structure Theory Group in Tampere University.

This repository contains two script files. Their documentations contain more information about usage. The scripts are built only to fulfill temporal goal, and they are not general purpose solutions.
* `read_some_obervables.py` 
    * Reads observables from rtpi-code, and extracts information in text and plots. Observables are: 
        * mean energy & energy by time step
        * density distribution mean & density distribution by time step & radial density
* ` fock_darwin_energy_states.py `
    * Calculates Fock-Darvin (FD) energy states analytically. FD system means 2d parabolic QD in magnetic field. Useful article: [https://arxiv.org/pdf/1703.06634.pdf]()
    * This file is not related to rtpi, but it can be used to verify quantum dot system results.